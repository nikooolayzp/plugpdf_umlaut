/****************************************************************************
 **
 ** Copyright (c) 2015 ePapyrus, Inc. All rights reserved.
 **
 ** TableViewController.m
 ** UsingPlugPDFDocumentViewController
 **
 ** This file is part of PlugPDF for iOS project.
 **
 ****************************************************************************/

#import "TableViewController.h"

#import "PlugPDF/DocumentViewController.h"
#import "PlugPDF/Document.h"

@interface TableViewController ()

@property (nonatomic, strong) NSString *path;

@end

@implementation TableViewController

#pragma mark - How to open PDF view via PlugPDFDocumentViewController

- (void)openPlugPDFDocumentViewControllerWithPath: (NSString*)path
                                         password: (NSString*)password
{
    PlugPDFDocumentViewController *viewController = nil;
    @try {
        NSString *name = [path lastPathComponent];
        viewController = [PlugPDFDocumentViewController initWithPath: path
                                                            password: nil
                                                               title: name];
    }
    @catch (NSException *exception) {
        NSLog(@"%@ %@", exception.name, exception.description);
    }
    
    if (viewController) {
        if ([[UIDevice currentDevice].systemVersion hasPrefix:@"7"] ||
            [[UIDevice currentDevice].systemVersion hasPrefix:@"8"]) {
            [viewController setAutomaticallyAdjustsScrollViewInsets: NO];
        }
        [[[self navigationController] navigationBar] setTranslucent: YES];
        [[self navigationController] pushViewController: viewController
                                               animated: YES];
    }
}

#pragma mark - Table view data source

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        self.path = @"";
    }
    return self;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSLog(@"Documents: %@", [paths objectAtIndex: 0]);
    NSArray *files = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:
                      [paths objectAtIndex: 0] error: Nil];
    return files.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"CellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle: UITableViewCellStyleDefault
                                      reuseIdentifier: cellIdentifier];
    }
    [[cell textLabel] setText: [[self filePathForRowAtIndexPath: indexPath] lastPathComponent]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *path = [self filePathForRowAtIndexPath: indexPath];
    if ([path isEqualToString: @""]) return;
    
    if ([PlugPDFDocument hasUserPasswordAtPath: path]) {
        self.path = path;
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Password"
                                                        message: nil
                                                       delegate: self
                                              cancelButtonTitle: @"cancel"
                                              otherButtonTitles: @"open", nil];
        [alert setAlertViewStyle: UIAlertViewStyleSecureTextInput];
        [alert show];
        return;
    }
    
    [self openPlugPDFDocumentViewControllerWithPath: path
                                           password: @""];
}

- (NSString*)filePathForRowAtIndexPath: (NSIndexPath *)indexPath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSArray *files = [[NSFileManager defaultManager] contentsOfDirectoryAtPath: [paths objectAtIndex: 0] error: Nil];

    NSString *path = @"";
    if (files.count > indexPath.row) {
        NSString *name = [files objectAtIndex: indexPath.row];
        if ([[[name pathExtension] uppercaseString] isEqualToString: @"PDF"]) {
            path = [[paths objectAtIndex: 0] stringByAppendingPathComponent: name];
        }
    }
    return path;
}

#pragma mark - UIAlertView

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    
    if([title isEqualToString:@"open"]) {
        NSString *password = [alertView textFieldAtIndex:0].text;
        [self openPlugPDFDocumentViewControllerWithPath: self.path
                                               password: password];
    }
}

@end
