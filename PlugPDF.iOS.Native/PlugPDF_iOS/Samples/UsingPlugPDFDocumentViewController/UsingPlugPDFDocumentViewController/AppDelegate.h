/****************************************************************************
 **
 ** Copyright (c) 2015 ePapyrus, Inc. All rights reserved.
 **
 ** AppDelegate.h
 ** UsingPlugPDFDocumentViewController
 **
 ** This file is part of PlugPDF for iOS project.
 **
 ****************************************************************************/

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

