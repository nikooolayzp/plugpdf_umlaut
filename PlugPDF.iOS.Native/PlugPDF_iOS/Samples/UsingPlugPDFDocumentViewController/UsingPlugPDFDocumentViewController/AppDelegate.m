/****************************************************************************
 **
 ** Copyright (c) 2015 ePapyrus, Inc. All rights reserved.
 **
 ** AppDelegate.m
 ** UsingPlugPDFDocumentViewController
 **
 ** This file is part of PlugPDF for iOS project.
 **
 ****************************************************************************/

#import "AppDelegate.h"

#import "TableViewController.h"

#import "PlugPDF/PlugPDF.h"
#import "PlugPDF/Version.h"
#import "PlugPDF/NavigationController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    @try {
        PlugPDFInit("38B9BEA24G6942B8EDB5G2A7HF49B2AH3GED5H3E8B6599ABCA6HFBF5");
        enableUncaughtExceptionHandler();
    }
    @catch (NSException *exception) {
        NSLog(@"Exception %@", exception.description);
        return NO;
    }
    
    NSLog(@"PlugPDF Version %@", [PlugPDFVersion getVersionName]);
    
    [self installResource: @"Politikerhörna_stadsbiblioteket434.pdf"];
    
    PlugPDFNavigationController *navigationController =
    [[PlugPDFNavigationController alloc] initWithRootViewController:
     [[TableViewController alloc] init]];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window setRootViewController: navigationController];
    [self.window makeKeyAndVisible];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    PlugPDFUninit();
}

- (void)installResource: (NSString*)name
{
    NSString *src = [[NSBundle mainBundle] pathForResource: [name stringByDeletingPathExtension]
                                                    ofType: [name pathExtension]];
    NSString *dest = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)
                       objectAtIndex:0] stringByAppendingPathComponent: name];
    
    NSError *error = nil;
    if ([[NSFileManager defaultManager] fileExistsAtPath: dest]) {
        if (![[NSFileManager defaultManager] removeItemAtPath: dest
                                                        error: &error]) {
            NSLog(@"Error description-%@ \n", [error localizedDescription]);
            NSLog(@"Error reason-%@", [error localizedFailureReason]);
        }
    }
    if (![[NSFileManager defaultManager] copyItemAtPath: src
                                                 toPath: dest
                                                  error: &error]) {
        NSLog(@"Error description-%@ \n", [error localizedDescription]);
        NSLog(@"Error reason-%@", [error localizedFailureReason]);
    }
}

@end
