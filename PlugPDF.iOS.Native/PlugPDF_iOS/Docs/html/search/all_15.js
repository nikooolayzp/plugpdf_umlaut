var searchData=
[
  ['zoomflag',['zoomFlag',['../interface_plug_p_d_f_document_view.html#a17baadbf0cc3be1d97f8745552629c13',1,'PlugPDFDocumentView']]],
  ['zoomscale',['zoomScale',['../interface_plug_p_d_f_document_view.html#a00dec511c2d68c3a3851944f3bce12c6',1,'PlugPDFDocumentView::zoomScale()'],['../interface_plug_p_d_f_document_view_controller.html#a2dc9e6c7db04f97de39f439b70f673c6',1,'PlugPDFDocumentViewController::zoomScale()']]],
  ['zoomscale_3a',['zoomScale:',['../interface_plug_p_d_f_document_view.html#ad015fc5937b129e03a174044628bf452',1,'PlugPDFDocumentView']]],
  ['zoomtopoint_3ascale_3aanimated_3a',['zoomToPoint:scale:animated:',['../interface_plug_p_d_f_document_view.html#abde2b373c2ad7a4466c9689ea8a6c169',1,'PlugPDFDocumentView::zoomToPoint:scale:animated:()'],['../interface_plug_p_d_f_document_view_controller.html#a23635d593e4ec41f4dd2e445b8f1030d',1,'PlugPDFDocumentViewController::zoomToPoint:scale:animated:()'],['../interface_plug_p_d_f_page_view.html#a5e59d47ff539775e027bf0402dcf2253',1,'PlugPDFPageView::zoomToPoint:scale:animated:()']]]
];
