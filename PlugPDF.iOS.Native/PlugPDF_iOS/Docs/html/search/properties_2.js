var searchData=
[
  ['cellwidth',['cellWidth',['../interface_plug_p_d_f_document_view.html#afee0a745a707727b2d18a4e1960483a3',1,'PlugPDFDocumentView']]],
  ['circleannotbordercolor',['circleAnnotBorderColor',['../interface_plug_p_d_f_document_view_controller.html#a3abc95dd0a1f2d71629a5c3002c3043c',1,'PlugPDFDocumentViewController']]],
  ['circleannotcolor',['circleAnnotColor',['../interface_plug_p_d_f_document_view_controller.html#aa995ac3c46bc9086a1c81570f4480a0a',1,'PlugPDFDocumentViewController']]],
  ['circleannotwidth',['circleAnnotwidth',['../interface_plug_p_d_f_document_view_controller.html#a3d1f18b6fb7713fa06f80ad76ba8269a',1,'PlugPDFDocumentViewController']]],
  ['color',['color',['../interface_ink_annot.html#a2ed66e4e44994190c2e1ebfa8a24333d',1,'InkAnnot::color()'],['../interface_plug_p_d_f_text_field.html#a7cac88609e399b658399d92d22b33773',1,'PlugPDFTextField::color()'],['../interface_text_markup_annot.html#abda7ee46a31c3feb75a055ae3844e8da',1,'TextMarkupAnnot::color()']]],
  ['contents',['contents',['../interface_free_text_annot.html#a3e9e9f0e9400110fb02491797df1a266',1,'FreeTextAnnot::contents()'],['../interface_note_annot.html#a0479fbdcb59069335fbc071e3688d4fc',1,'NoteAnnot::contents()']]]
];
