var searchData=
[
  ['cellwidth',['cellWidth',['../interface_plug_p_d_f_document_view.html#afee0a745a707727b2d18a4e1960483a3',1,'PlugPDFDocumentView']]],
  ['checkboxfieldvaluewithpage_3atitle_3a',['checkBoxFieldValueWithPage:title:',['../interface_plug_p_d_f_document.html#a205745ac91325c152046f58e7b395500',1,'PlugPDFDocument']]],
  ['checked',['checked',['../interface_plug_p_d_f_check_box_field.html#af15a90f9d864737c32e0c60a5a18fe01',1,'PlugPDFCheckBoxField']]],
  ['circleannot',['CircleAnnot',['../interface_circle_annot.html',1,'']]],
  ['circleannotbordercolor',['circleAnnotBorderColor',['../interface_plug_p_d_f_document_view_controller.html#a3abc95dd0a1f2d71629a5c3002c3043c',1,'PlugPDFDocumentViewController']]],
  ['circleannotcolor',['circleAnnotColor',['../interface_plug_p_d_f_document_view_controller.html#aa995ac3c46bc9086a1c81570f4480a0a',1,'PlugPDFDocumentViewController']]],
  ['circleannotwidth',['circleAnnotwidth',['../interface_plug_p_d_f_document_view_controller.html#a3d1f18b6fb7713fa06f80ad76ba8269a',1,'PlugPDFDocumentViewController']]],
  ['clearallfield_3a',['clearAllField:',['../interface_plug_p_d_f_document.html#a2adc0e440e859af0c547a4d7db2d6757',1,'PlugPDFDocument']]],
  ['clearfieldwithpage_3atitle_3a',['clearFieldWithPage:title:',['../interface_plug_p_d_f_document.html#ad2600b22fc3293f61a4b86f14f0a9d08',1,'PlugPDFDocument']]],
  ['clearvalue',['clearValue',['../interface_plug_p_d_f_base_field.html#aa3d48d964ec56cbb57472086bbcd7694',1,'PlugPDFBaseField']]],
  ['color',['color',['../interface_ink_annot.html#a2ed66e4e44994190c2e1ebfa8a24333d',1,'InkAnnot::color()'],['../interface_plug_p_d_f_text_field.html#a7cac88609e399b658399d92d22b33773',1,'PlugPDFTextField::color()'],['../interface_text_markup_annot.html#abda7ee46a31c3feb75a055ae3844e8da',1,'TextMarkupAnnot::color()']]],
  ['comboboxfieldvaluewithpage_3atitle_3a',['comboBoxFieldValueWithPage:title:',['../interface_plug_p_d_f_document.html#a51780f46f33f0a349e855c3fd668dd9c',1,'PlugPDFDocument']]],
  ['containsoutline',['containsOutline',['../interface_plug_p_d_f_document.html#ab890f9eb6afbc3f92257426ad41847d6',1,'PlugPDFDocument']]],
  ['contents',['contents',['../interface_free_text_annot.html#a3e9e9f0e9400110fb02491797df1a266',1,'FreeTextAnnot::contents()'],['../interface_note_annot.html#a0479fbdcb59069335fbc071e3688d4fc',1,'NoteAnnot::contents()']]],
  ['copycontentpermission',['copyContentPermission',['../interface_plug_p_d_f_document.html#a2b4be41911dab1200edfefad37593560',1,'PlugPDFDocument::copyContentPermission()'],['../interface_plug_p_d_f_document_view_controller.html#ad96524a3831283895a8114c79bfb69eb',1,'PlugPDFDocumentViewController::copyContentPermission()']]],
  ['createemptydocument_3asize_3a',['createEmptyDocument:size:',['../interface_plug_p_d_f_document.html#af602242d26176167edd8386d6af4bac0',1,'PlugPDFDocument']]],
  ['createsignaturefield_3adestpath_3acontentpath_3apassword_3apageidx_3aname_3areason_3alocation_3a',['createSignatureField:destPath:contentPath:password:pageIdx:name:reason:location:',['../interface_plug_p_d_f_document.html#acf45c2fa2594741a4a76a54c43cc8b59',1,'PlugPDFDocument']]]
];
