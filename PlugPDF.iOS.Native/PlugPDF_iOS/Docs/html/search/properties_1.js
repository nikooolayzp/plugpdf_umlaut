var searchData=
[
  ['bookmarktitle',['bookmarkTitle',['../interface_plug_p_d_f_document_view_controller.html#a28c3924aa733ae1573869db6f35d264c',1,'PlugPDFDocumentViewController']]],
  ['bordercolor',['borderColor',['../interface_plug_p_d_f_base_field.html#abaa1f880238431f48d1f1253004a903d',1,'PlugPDFBaseField::borderColor()'],['../interface_square_annot.html#a3d27a7b27b5b6e3762e98869d899ef2e',1,'SquareAnnot::borderColor()']]],
  ['borderhidden',['borderHidden',['../interface_plug_p_d_f_base_field.html#a876129da99871e3922f621a036cdcd7d',1,'PlugPDFBaseField']]],
  ['borderstyle',['borderStyle',['../interface_square_annot.html#ace0e14d309418b4587ad2dc049eec7f8',1,'SquareAnnot']]],
  ['borderwidth',['borderWidth',['../interface_square_annot.html#ab23648be23bb6a0195d5b9525ae21b93',1,'SquareAnnot']]]
];
