var searchData=
[
  ['radiobuttonfieldvaluewithpage_3atitle_3aname_3a',['radioButtonFieldValueWithPage:title:name:',['../interface_plug_p_d_f_document.html#a9f75f0a8b0aadff81ca7aad208fa751c',1,'PlugPDFDocument']]],
  ['readdigesttoken_3acontentpath_3apassword_3apageidx_3afieldtitle_3a',['readDigestToken:contentPath:password:pageIdx:fieldTitle:',['../interface_plug_p_d_f_document.html#ac3ef272ea57a786f783bef74bbde4c94',1,'PlugPDFDocument']]],
  ['rect',['rect',['../interface_base_annot.html#a55756b2719e4081788cda2789286550a',1,'BaseAnnot']]],
  ['refresh',['refresh',['../interface_base_annot.html#a5def9018ee448cdef823d091860cf3c8',1,'BaseAnnot::refresh()'],['../interface_ink_annot.html#ae93b72fd9bb1730c8dbd75a41a1f1b93',1,'InkAnnot::refresh()']]],
  ['releasepage_3a',['releasePage:',['../interface_plug_p_d_f_document.html#ae873171af254c13ff0a7de7ca63754a3',1,'PlugPDFDocument']]],
  ['releasepageiscalled',['releasePageisCalled',['../interface_plug_p_d_f_document_view.html#ae26023af485739b044dc8184af16bc83',1,'PlugPDFDocumentView']]],
  ['releasepages',['releasePages',['../interface_plug_p_d_f_document_view.html#ada5027897124d090253e2bedfe95f47c',1,'PlugPDFDocumentView']]],
  ['releasetool_3a',['releaseTool:',['../interface_plug_p_d_f_document_view.html#a89a6c7d3c88e5c33d790c5aeab5715bf',1,'PlugPDFDocumentView::releaseTool:()'],['../interface_plug_p_d_f_document_view_controller.html#ae1e44c6eba73bb27f800e91b9ee11739',1,'PlugPDFDocumentViewController::releaseTool:()'],['../interface_plug_p_d_f_page_view.html#a432cc6bc91de2e7a228600f6ceddbbc6',1,'PlugPDFPageView::releaseTool:()']]],
  ['releasetools',['releaseTools',['../interface_plug_p_d_f_document_view.html#a0f96226cea066403d8d4289ca92b1670',1,'PlugPDFDocumentView::releaseTools()'],['../interface_plug_p_d_f_document_view_controller.html#acaa3b7c5eda6974d59157377e9fbfafd',1,'PlugPDFDocumentViewController::releaseTools()']]],
  ['removeaccent_3a',['removeAccent:',['../interface_plug_p_d_f_document.html#a2135423621577795e739927005b17816',1,'PlugPDFDocument']]],
  ['removeannot',['removeAnnot',['../interface_base_annot.html#a12942bbf29c668646466099e060106c1',1,'BaseAnnot']]],
  ['removeannot_3a',['removeAnnot:',['../interface_plug_p_d_f_document.html#aa916c5ec66db5670613187425fab060a',1,'PlugPDFDocument::removeAnnot:()'],['../interface_plug_p_d_f_document_view.html#a3a6b448a269aac9a631c9739cebc8b12',1,'PlugPDFDocumentView::removeAnnot:()'],['../interface_plug_p_d_f_document_view_controller.html#a1296919842394a6c5c51f814bd327218',1,'PlugPDFDocumentViewController::removeAnnot:()'],['../interface_plug_p_d_f_page_view.html#ade4c30ed74ccf84a5fd662c3a7ece1fa',1,'PlugPDFPageView::removeAnnot:()']]],
  ['removeoutlineitem_3a',['removeOutlineItem:',['../interface_plug_p_d_f_document.html#aed2b016fd79543856723ce289fbb7424',1,'PlugPDFDocument::removeOutlineItem:()'],['../interface_plug_p_d_f_document_view_controller.html#af1b99bc14a4ab17b3410c2c9792e6bed',1,'PlugPDFDocumentViewController::removeOutlineItem:()']]],
  ['resetzoom_3a',['resetZoom:',['../interface_plug_p_d_f_document_view.html#a8824c5d51bce2f277e37863234dddfab',1,'PlugPDFDocumentView::resetZoom:()'],['../interface_plug_p_d_f_document_view_controller.html#a6382f10670c219e0a3509e34dca48a0c',1,'PlugPDFDocumentViewController::resetZoom:()'],['../interface_plug_p_d_f_page_view.html#a2cc94d12c7bfa9e9a68b2ae0a0581cc7',1,'PlugPDFPageView::resetZoom:()']]],
  ['richmediaannot',['RichMediaAnnot',['../interface_rich_media_annot.html',1,'']]]
];
