var searchData=
[
  ['pagecount',['pageCount',['../interface_plug_p_d_f_document.html#a74e0c3084632f15fe0a13358b45b3672',1,'PlugPDFDocument::pageCount()'],['../interface_plug_p_d_f_document_view.html#a0982bac59e7bfcbde230853e0363d922',1,'PlugPDFDocumentView::pageCount()'],['../interface_plug_p_d_f_document_view_controller.html#a708315dc0cbd342eb7b0a4815924cfd5',1,'PlugPDFDocumentViewController::pageCount()']]],
  ['pagedidchange_3apageidx_3a',['pageDidChange:pageIdx:',['../protocol_plug_p_d_f_document_view_event_delegate-p.html#af29e88a1f0f44ce278e86533a1c6572b',1,'PlugPDFDocumentViewEventDelegate-p']]],
  ['pageidx',['pageIdx',['../interface_plug_p_d_f_document_view_controller.html#aa3436993ead151aeee3b0780ec9907a8',1,'PlugPDFDocumentViewController']]],
  ['pageidxwithcontentoffset',['pageIdxWithContentOffset',['../interface_plug_p_d_f_document_view.html#aa6f056b3c9e38da35c5889a9faa08817',1,'PlugPDFDocumentView']]],
  ['pagesize_3a',['pageSize:',['../interface_plug_p_d_f_document.html#a5157feb7576a9c5b0344c699c0609421',1,'PlugPDFDocument']]],
  ['pageview_3a',['pageView:',['../interface_plug_p_d_f_document_view.html#a2d0ada96725b7949558d143ca95ca09d',1,'PlugPDFDocumentView']]],
  ['pagewillchange_3apageidx_3a',['pageWillChange:pageIdx:',['../protocol_plug_p_d_f_document_view_event_delegate-p.html#ac41f66da3f13b07454b99728d26b3eca',1,'PlugPDFDocumentViewEventDelegate-p']]],
  ['path',['path',['../interface_plug_p_d_f_document_view_controller.html#a23fa3e4f1ade39255cfcd18e7cdd9e28',1,'PlugPDFDocumentViewController']]],
  ['printpermission',['printPermission',['../interface_plug_p_d_f_document.html#ac97ad311adbe159cd717d8243022b241',1,'PlugPDFDocument::printPermission()'],['../interface_plug_p_d_f_document_view_controller.html#a8f73276a315613ea430dd2a1b44f511c',1,'PlugPDFDocumentViewController::printPermission()']]]
];
