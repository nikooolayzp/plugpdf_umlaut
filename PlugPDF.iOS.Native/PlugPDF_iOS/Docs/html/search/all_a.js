var searchData=
[
  ['lastdisplaymode',['lastDisplayMode',['../interface_plug_p_d_f_document_view.html#a4f4ab89c836052923ed833bfb3aa6624',1,'PlugPDFDocumentView::lastDisplayMode()'],['../interface_plug_p_d_f_document_view_controller.html#a7e062e7128e8e7fc85bc49d8e29304d2',1,'PlugPDFDocumentViewController::lastDisplayMode()']]],
  ['lineannot',['LineAnnot',['../interface_line_annot.html',1,'']]],
  ['lines',['lines',['../interface_ink_annot.html#a27293d6b08a3a38aa59c4d80cda1d84c',1,'InkAnnot']]],
  ['linkannot',['LinkAnnot',['../interface_link_annot.html',1,'']]],
  ['loadannots',['loadAnnots',['../interface_plug_p_d_f_page_view.html#a7b74a4357aae3638932691b9400e40b3',1,'PlugPDFPageView']]],
  ['loadannots_3a',['loadAnnots:',['../interface_plug_p_d_f_document.html#a0749d379122a2633c15cf5851b289652',1,'PlugPDFDocument']]],
  ['longpress',['longPress',['../interface_plug_p_d_f_page_view.html#ac611fd898e6d6e8daa675d38f5dbfa8c',1,'PlugPDFPageView']]]
];
