var searchData=
[
  ['editoutlineitem_3atitle_3apageidx_3a',['editOutlineItem:title:pageIdx:',['../interface_plug_p_d_f_document.html#a05f768c81b55dabdcd8c0eb43cd01565',1,'PlugPDFDocument::editOutlineItem:title:pageIdx:()'],['../interface_plug_p_d_f_document_view_controller.html#adbf31f55a6ec7c5d952d5bffe5ce2411',1,'PlugPDFDocumentViewController::editOutlineItem:title:pageIdx:()']]],
  ['encryptuserpassword_3aownerpassword_3apermission_3a',['encryptUserPassword:OwnerPassword:permission:',['../interface_plug_p_d_f_document.html#ad057bee9d84460523baa10ce51374a36',1,'PlugPDFDocument::encryptUserPassword:OwnerPassword:permission:()'],['../interface_plug_p_d_f_document_view_controller.html#a991e4e62a2caf6bb53ec2176ce4caf88',1,'PlugPDFDocumentViewController::encryptUserPassword:OwnerPassword:permission:()']]],
  ['exportannottoxfdf_3a',['exportAnnotToXFDF:',['../interface_plug_p_d_f_document.html#a3e6bab9bf6c5a6541179f923aa89fa5d',1,'PlugPDFDocument::exportAnnotToXFDF:()'],['../interface_plug_p_d_f_document_view.html#a6395feb97b3bd7c618e283130fbbfe0d',1,'PlugPDFDocumentView::exportAnnotToXFDF:()'],['../interface_plug_p_d_f_document_view_controller.html#a6fa825e4da4e2fe50a4ec86df3d68217',1,'PlugPDFDocumentViewController::exportAnnotToXFDF:()']]],
  ['exportannottoxfdf_3asrcpath_3apassword_3a',['exportAnnotToXFDF:srcPath:password:',['../interface_plug_p_d_f_document.html#ab9de01f444e6445d88f5361b4ca2c791',1,'PlugPDFDocument']]],
  ['extractpermission',['extractPermission',['../interface_plug_p_d_f_document.html#a2e17780268fa4625bfe3c84174ffad7a',1,'PlugPDFDocument::extractPermission()'],['../interface_plug_p_d_f_document_view_controller.html#af35373668e33b8b6aaa5f56945a79971',1,'PlugPDFDocumentViewController::extractPermission()']]],
  ['extracttext_3ainrect_3a',['extractText:inRect:',['../interface_plug_p_d_f_document.html#a86d3b8bed2afe73e1e42ae5a02566084',1,'PlugPDFDocument']]],
  ['extracttextandrects_3ainrect_3a',['extractTextAndRects:inRect:',['../interface_plug_p_d_f_document.html#af5396acefcfba517c3ea1119c4bade11',1,'PlugPDFDocument']]],
  ['extracttextandrectsinrect_3awithoffset_3a',['extractTextAndRectsInRect:withOffset:',['../interface_plug_p_d_f_page_view.html#a6f46dbaf3da5b5af6a427e9ee2d63b75',1,'PlugPDFPageView']]],
  ['extracttextinrect_3awithoffset_3a',['extractTextInRect:withOffset:',['../interface_plug_p_d_f_page_view.html#ad8ccf0f7d0d67d66498a20a80b31568b',1,'PlugPDFPageView']]],
  ['extracttextsrc_3apassword_3a',['extractTextSrc:password:',['../interface_plug_p_d_f_document.html#aa528452ffefdee6bbe371fa30142fdcd',1,'PlugPDFDocument']]],
  ['extracttextsrc_3apassword_3apageidx_3arect_3a',['extractTextSrc:password:pageIdx:rect:',['../interface_plug_p_d_f_document.html#a1665f13462997545e3a120c28fe154f7',1,'PlugPDFDocument']]]
];
