var searchData=
[
  ['fields_3a',['fields:',['../interface_plug_p_d_f_document.html#a46600bde78a0b3e4ef78268e1de7d00e',1,'PlugPDFDocument']]],
  ['fields_3atitle_3a',['fields:title:',['../interface_plug_p_d_f_document.html#a66baa4fd1668156da88b28981d59b156',1,'PlugPDFDocument']]],
  ['fieldstate',['fieldState',['../interface_plug_p_d_f_base_field.html#aab5f344a4c05965980938357312026c6',1,'PlugPDFBaseField']]],
  ['fieldstatewithpage_3atitle_3a',['fieldStateWithPage:title:',['../interface_plug_p_d_f_document.html#ac150443b989726b6bf0d1a76ac2defc8',1,'PlugPDFDocument']]],
  ['fieldstatewithpage_3atitle_3aname_3a',['fieldStateWithPage:title:name:',['../interface_plug_p_d_f_document.html#a7b9c68a9488f5b81f212893b87128fcf',1,'PlugPDFDocument']]],
  ['fillfiledpermission',['fillFiledPermission',['../interface_plug_p_d_f_document.html#a6793f7a1da80ebc50b2cbec92ec7db0c',1,'PlugPDFDocument::fillFiledPermission()'],['../interface_plug_p_d_f_document_view_controller.html#a995db0545982924ec1e742d0377336b1',1,'PlugPDFDocumentViewController::fillFiledPermission()']]],
  ['findstring_3apage_3a',['findString:page:',['../interface_plug_p_d_f_document.html#a480fe1ebd7661fe277a4a1e14def07b0',1,'PlugPDFDocument']]],
  ['flattenannotations',['flattenAnnotations',['../interface_plug_p_d_f_document.html#a0f03b121a747a066d23dff7038dc3f5e',1,'PlugPDFDocument::flattenAnnotations()'],['../interface_plug_p_d_f_document_view_controller.html#a399c9101a729bda44c9b594cda168b81',1,'PlugPDFDocumentViewController::flattenAnnotations()']]],
  ['flattenformfields_3ausecustomappearance_3a',['flattenFormFields:useCustomAppearance:',['../interface_plug_p_d_f_document_view_controller.html#af3b57ecf334ad8ac8918b29f7d5af8d8',1,'PlugPDFDocumentViewController']]],
  ['',['',['../interface_plug_p_d_f_document.html#a3617cc1839ecfa74080027a5d4362edc',1,'PlugPDFDocument']]]
];
