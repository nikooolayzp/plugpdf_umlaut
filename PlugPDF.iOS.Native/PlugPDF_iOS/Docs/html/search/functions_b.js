var searchData=
[
  ['maxzoomscale',['maxZoomScale',['../interface_plug_p_d_f_document_view.html#a508326dab34c19ba690b0bfd4fa603b3',1,'PlugPDFDocumentView::maxZoomScale()'],['../interface_plug_p_d_f_document_view_controller.html#a07ce8e091f5ad3699d31159f7103efa4',1,'PlugPDFDocumentViewController::maxZoomScale()']]],
  ['mergefiles_3apassword_3aoutfile_3a',['mergeFiles:password:outFile:',['../interface_plug_p_d_f_document.html#afaec0d34d980dfd738cbf3bbab429338',1,'PlugPDFDocument']]],
  ['minzoomscale',['minZoomScale',['../interface_plug_p_d_f_document_view.html#a8e809dafd4c360eff54ab7e3f8486ae4',1,'PlugPDFDocumentView::minZoomScale()'],['../interface_plug_p_d_f_document_view_controller.html#a2d8aebfd2f0ab58adccd00b028faff0d',1,'PlugPDFDocumentViewController::minZoomScale()']]],
  ['minzoomscale_3a',['minZoomScale:',['../interface_plug_p_d_f_document_view.html#ae018862d4c4ed93a7f84b9d3b37634fb',1,'PlugPDFDocumentView']]],
  ['modifyannotpermission',['modifyAnnotPermission',['../interface_plug_p_d_f_document.html#adbef197260698e60cb88dbce1bc9be8c',1,'PlugPDFDocument::modifyAnnotPermission()'],['../interface_plug_p_d_f_document_view_controller.html#a6d42a91da8fa3cc92287b0e46f8c1d10',1,'PlugPDFDocumentViewController::modifyAnnotPermission()']]],
  ['modifycontentpermission',['modifyContentPermission',['../interface_plug_p_d_f_document.html#aecceb1ed8dd3a9860f8b634fca44c213',1,'PlugPDFDocument::modifyContentPermission()'],['../interface_plug_p_d_f_document_view_controller.html#a3bd7aec2f7f1391bd0d4ea35c45ffa5e',1,'PlugPDFDocumentViewController::modifyContentPermission()']]]
];
