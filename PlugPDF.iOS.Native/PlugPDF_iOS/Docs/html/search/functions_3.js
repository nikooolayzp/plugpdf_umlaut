var searchData=
[
  ['deletehitview',['deleteHitView',['../interface_plug_p_d_f_document_view.html#a8f152dba17a4b30b67e79467a2fe0868',1,'PlugPDFDocumentView::deleteHitView()'],['../interface_plug_p_d_f_page_view.html#a2750f0cb8247be020f2072c623fd61e1',1,'PlugPDFPageView::deleteHitView()']]],
  ['displaymode',['displayMode',['../interface_plug_p_d_f_document_view.html#ae7925fd694e081a5726ea8ad45001e1c',1,'PlugPDFDocumentView::displayMode()'],['../interface_plug_p_d_f_document_view_controller.html#a1504df5602b871a968b931f93f307b5a',1,'PlugPDFDocumentViewController::displayMode()']]],
  ['documentassemblypermission',['documentAssemblyPermission',['../interface_plug_p_d_f_document.html#a9ab022ab77bf8123e9ce3f62df27ede7',1,'PlugPDFDocument::documentAssemblyPermission()'],['../interface_plug_p_d_f_document_view_controller.html#a7014778c322f8ea8db1c7706dac1c2d8',1,'PlugPDFDocumentViewController::documentAssemblyPermission()']]],
  ['drawpage2_3asize_3a',['drawPage2:size:',['../interface_plug_p_d_f_document.html#a242285b5230caae2dffa0189a447cb46',1,'PlugPDFDocument']]],
  ['drawpage_3asize_3a',['drawPage:size:',['../interface_plug_p_d_f_document.html#a27c76db91e1260983895d2c956782eb3',1,'PlugPDFDocument']]],
  ['drawrect_3afield_3a',['drawRect:field:',['../protocol_plug_p_d_f_check_box_field_draw_delegate-p.html#a0c67131849f9dc6ee37a2ceffd61de2f',1,'PlugPDFCheckBoxFieldDrawDelegate-p']]],
  ['drawrect_3afieldstate_3a',['drawRect:fieldState:',['../interface_plug_p_d_f_base_field.html#a55574871c8c64e89980106f043c50108',1,'PlugPDFBaseField']]],
  ['drawtile_3asize_3atilerect_3azoom_3a',['drawTile:size:tileRect:zoom:',['../interface_plug_p_d_f_document.html#a0508e788a72bf9811733adebc07eb7f8',1,'PlugPDFDocument']]]
];
