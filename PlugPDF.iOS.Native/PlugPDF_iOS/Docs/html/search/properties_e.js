var searchData=
[
  ['scale',['scale',['../interface_base_annot.html#a4e0fc857c810c654e41305df0fa70b87',1,'BaseAnnot']]],
  ['source',['source',['../interface_rich_media_annot.html#af1e267f0493241588d34b80539cf12ce',1,'RichMediaAnnot']]],
  ['squareannotbordercolor',['squareAnnotBorderColor',['../interface_plug_p_d_f_document_view_controller.html#a21949fab2dc22a3e41cdcea413b45904',1,'PlugPDFDocumentViewController']]],
  ['squareannotcolor',['squareAnnotColor',['../interface_plug_p_d_f_document_view_controller.html#a512e16e8f268e02d990353314b6fb85b',1,'PlugPDFDocumentViewController']]],
  ['squareannotwidth',['squareAnnotwidth',['../interface_plug_p_d_f_document_view_controller.html#ad3287f9fe61d82a695a189bdcb7d6916',1,'PlugPDFDocumentViewController']]],
  ['swipeleft',['swipeLeft',['../interface_plug_p_d_f_page_view.html#a4d349b8f387b3683056371e703d8cb2c',1,'PlugPDFPageView']]]
];
