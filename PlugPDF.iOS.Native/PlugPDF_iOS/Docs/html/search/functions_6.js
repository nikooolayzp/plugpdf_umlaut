var searchData=
[
  ['getmajorversion',['getMajorVersion',['../interface_plug_p_d_f_version.html#a9684aec9592bba0ffe63b20fbb039496',1,'PlugPDFVersion']]],
  ['getscale_3a',['getScale:',['../interface_base_annot.html#ae09bec269c2952e5ebebad87a0765e84',1,'BaseAnnot']]],
  ['getuseraccesspermissionswithprint_3amodifycontent_3acopycontent_3amodifyannot_3afillfiled_3aextract_3adocumentassembly_3a',['getUserAccessPermissionsWithPrint:modifyContent:copyContent:modifyAnnot:fillFiled:extract:documentAssembly:',['../interface_plug_p_d_f_document.html#a2e858960a51e681cfe5f0b209af74234',1,'PlugPDFDocument']]],
  ['getversionname',['getVersionName',['../interface_plug_p_d_f_version.html#aaf669b1593fa12ff5f4cc5520b98d96d',1,'PlugPDFVersion']]],
  ['gotopage_3afittoscreen_3a',['gotoPage:fitToScreen:',['../interface_plug_p_d_f_document_view.html#a2a8830092dd96e7a86754db20b63bc14',1,'PlugPDFDocumentView::gotoPage:fitToScreen:()'],['../interface_plug_p_d_f_document_view_controller.html#ac31165f6630d6d1ddd558a4a602edea1',1,'PlugPDFDocumentViewController::gotoPage:fitToScreen:()']]]
];
