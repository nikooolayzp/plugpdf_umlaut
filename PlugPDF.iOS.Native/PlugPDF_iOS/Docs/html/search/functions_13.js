var searchData=
[
  ['viewcontroller_3aonlongpress_3a',['viewController:onLongPress:',['../protocol_plug_p_d_f_annot_event_delegate-p.html#ad9dec109e5147557ae0a03c13298eb2d',1,'PlugPDFAnnotEventDelegate-p']]],
  ['viewcontroller_3aontapup_3a',['viewController:onTapUp:',['../protocol_plug_p_d_f_annot_event_delegate-p.html#a2ca6d381a23e4ac3248bec186be90bd2',1,'PlugPDFAnnotEventDelegate-p']]],
  ['viewdidappear_3aanimated_3a',['viewDidAppear:animated:',['../protocol_plug_p_d_f_document_view_controller_delegate-p.html#a8ccc6456fdda1a9438c919a8fa0ae6b6',1,'PlugPDFDocumentViewControllerDelegate-p']]],
  ['viewdiddisappear_3aanimated_3a',['viewDidDisappear:animated:',['../protocol_plug_p_d_f_document_view_controller_delegate-p.html#a57f76dd6459c6314ab36540bdd824340',1,'PlugPDFDocumentViewControllerDelegate-p']]],
  ['viewdidload_3a',['viewDidLoad:',['../protocol_plug_p_d_f_document_view_controller_delegate-p.html#a0cb9e27bb261e146c17698b4e69165c9',1,'PlugPDFDocumentViewControllerDelegate-p']]],
  ['viewwillappear_3aanimated_3a',['viewWillAppear:animated:',['../protocol_plug_p_d_f_document_view_controller_delegate-p.html#aed3d82cad0e1e5105c843cc701abeb0d',1,'PlugPDFDocumentViewControllerDelegate-p']]],
  ['viewwilldisappear_3aanimated_3a',['viewWillDisappear:animated:',['../protocol_plug_p_d_f_document_view_controller_delegate-p.html#aded67be34f6b07606378376c08279e4d',1,'PlugPDFDocumentViewControllerDelegate-p']]]
];
