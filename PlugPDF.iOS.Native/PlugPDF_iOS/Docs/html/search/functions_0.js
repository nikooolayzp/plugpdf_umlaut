var searchData=
[
  ['addannot_3a',['addAnnot:',['../interface_plug_p_d_f_page_view.html#a521e20e93784a3c30d2922b511b8649e',1,'PlugPDFPageView']]],
  ['addleftbarbuttonitem_3a',['addLeftBarButtonItem:',['../interface_plug_p_d_f_document_view_controller.html#a0ef2e7b612671a3d7024fe9c936e757d',1,'PlugPDFDocumentViewController']]],
  ['addoutlineitemwithtitle_3apageidx_3aparent_3aafter_3a',['addOutlineItemWithTitle:pageIdx:parent:after:',['../interface_plug_p_d_f_document.html#a3b773bb20044802f7b634fe4766231b0',1,'PlugPDFDocument::addOutlineItemWithTitle:pageIdx:parent:after:()'],['../interface_plug_p_d_f_document_view_controller.html#ab18409d820fe75bf35b4c3cb2b7d5764',1,'PlugPDFDocumentViewController::addOutlineItemWithTitle:pageIdx:parent:after:()']]],
  ['addrightbarbuttonitem_3a',['addRightBarButtonItem:',['../interface_plug_p_d_f_document_view_controller.html#aecf76a522b771830601cc5a688956269',1,'PlugPDFDocumentViewController']]],
  ['addsibling_3a',['addSibling:',['../interface_plug_p_d_f_radio_button_field.html#a5c6321563a9ec910e4a7b241313740d7',1,'PlugPDFRadioButtonField']]],
  ['annotisadded_3a',['AnnotisAdded:',['../protocol_plug_p_d_f_document_view_event_delegate-p.html#a43d749c9b1f4899860885793693c2dd3',1,'PlugPDFDocumentViewEventDelegate-p']]],
  ['annotisremoved_3a',['AnnotisRemoved:',['../protocol_plug_p_d_f_document_view_event_delegate-p.html#a656d98e2271fc9efd9471a1242ea8ed5',1,'PlugPDFDocumentViewEventDelegate-p']]],
  ['annots_3a',['annots:',['../interface_plug_p_d_f_document.html#a93aeef603d5a2ba7c32e64eaa26090ba',1,'PlugPDFDocument::annots:()'],['../interface_plug_p_d_f_document_view.html#abd1f7359dca36494d90acfdd5f9e878e',1,'PlugPDFDocumentView::annots:()'],['../interface_plug_p_d_f_document_view_controller.html#a2337f0d76b5cd691408c135a667983cd',1,'PlugPDFDocumentViewController::annots:()']]]
];
